import { Component, OnInit } from '@angular/core';

export interface PaymentElement {
  date: string;
  semester: string;
  value: string;
  transfer: string;
  afterOperation: string;
  description: string;
}

export interface ScholarshipElement {
  date: string;
  semester: string;
  value: string;
}

const FEES_ELEMENT_DATA: PaymentElement[] = [
  {
    date: "30/11/2019",
    semester: "1",
    value: "-22 zł",
    transfer: "outcomming on time",
    afterOperation: "0 zł",
    description: "Student card"
  },
  {
    date: "30/11/2019",
    semester: "1",
    value: "+22 zł",
    transfer: "incomming",
    afterOperation: "22 zł",
    description: "Fee for a student card"
  }
];

const SCHOLARSHIPS_ELEMENT_DATA: ScholarshipElement[] = [
  {
    date: "30/02/2021",
    semester: "3",
    value: "550 zł",
  },
  {
    date: "30/01/2021",
    semester: "3",
    value: "550 zł",
  },

  {
    date: "30/12/2020",
    semester: "3",
    value: "550 zł",
  },

  {
    date: "30/11/2020",
    semester: "3",
    value: "550 zł",
  },
  {
    date: "30/10/2020",
    semester: "3",
    value: "550 zł",
  },
];

@Component({
  selector: 'app-student-fees',
  templateUrl: './student-fees.component.html',
  styleUrls: ['./student-fees.component.css']
})
export class StudentFeesComponent implements OnInit {

  displayedFeesColumns: string[] = ['date', 'semester', 'value', 'transfer', 'afterOperation', 'description'];
  feesDataSource = FEES_ELEMENT_DATA;

  displayedSholarshipColumns: string[] = ['date', 'semester', 'value'];
  scholarshipDataSource = SCHOLARSHIPS_ELEMENT_DATA;

  constructor() { }

  ngOnInit(): void {
  }

}
