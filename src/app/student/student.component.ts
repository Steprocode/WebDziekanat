import { Component, OnInit } from '@angular/core';
import { StudentGeneralComponent } from '../student-general/student-general.component';
import { StudentReportComponent } from '../student-report/student-report.component';
import { StudentCoursesComponent } from '../student-courses/student-courses.component';
  
@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {


  constructor() { }

  ngOnInit(): void {
  }

}
