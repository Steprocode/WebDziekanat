import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StudentComponent } from './student/student.component';
import { TermsComponent } from './terms/terms.component';
import { CourseCatalogueComponent } from './course-catalogue/course-catalogue.component';
import { RegulationsComponent } from './regulations/regulations.component';
import { ApplicationsComponent } from './applications/applications.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

const routes: Routes = [
  { path: 'student', component: StudentComponent },
  { path: 'regulations', component: RegulationsComponent },
  { path: 'applications', component: ApplicationsComponent },
  { path: 'page-not-found', component: PageNotFoundComponent },
  { path: '', redirectTo: '/student', pathMatch: 'full' },
  { path: '**', redirectTo: '/page-not-found', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
