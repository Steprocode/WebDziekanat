import { Component, OnInit } from '@angular/core';

export interface GradeElement {
  courseNo: string;
  courseName: string;
  tutor: string;
  ects: string;
  grade1: string;
  grade2: string;
  grade3: string;
  paid: string;
}

const ELEMENT_DATA: GradeElement[] = [
  {
    courseNo: "09 821015 00",
    courseName: "Civic Knowledge and Engagement 4",
    tutor: "mgr Iwona Wróblewska",
    ects: "1",
    grade1: "-",
    grade2: "-",
    grade3: "-",
    paid: "no"
  },
  {
    courseNo: "02 636340 00",
    courseName: "Computer Aided Design",
    tutor: "dr inż. Witold Kubiak",
    ects: "3",
    grade1: "-",
    grade2: "-",
    grade3: "-",
    paid: "no"
  },
  {
    courseNo: "02 866131 00",
    courseName: "Diploma Proseminar",
    tutor: "dr hab. inż. Lidia Jackowska-Strumiłło",
    ects: "1",
    grade1: "-",
    grade2: "-",
    grade3: "-",
    paid: "no"
  },
  {
    courseNo: "02 526421 00",
    courseName: "GUI-programming",
    tutor: "dr inż. Jacek Nowakowski",
    ects: "3",
    grade1: "-",
    grade2: "-",
    grade3: "-",
    paid: "no"
  },
  {
    courseNo: "02 646214 00",
    courseName: "Internet of Things",
    tutor: "dr inż. Krzysztof Lichy",
    ects: "5",
    grade1: "-",
    grade2: "-",
    grade3: "-",
    paid: "no"
  },
  {
    courseNo: "02 556163 00",
    courseName: "Operating Systems",
    tutor: "dr hab. inż. Krzysztof Grudzień",
    ects: "5",
    grade1: "-",
    grade2: "-",
    grade3: "-",
    paid: "no"
  },
  {
    courseNo: "02 966203 00",
    courseName: "Professional Design Project",
    tutor: "dr hab. Laurent Babout",
    ects: "9",
    grade1: "-",
    grade2: "-",
    grade3: "-",
    paid: "no"
  },
  {
    courseNo: "02 646215 00",
    courseName: "Virtual Reality",
    tutor: "dr inż. Dorota Kamińska",
    ects: "3",
    grade1: "-",
    grade2: "-",
    grade3: "-",
    paid: "no"
  }
];

@Component({
  selector: 'app-student-report',
  templateUrl: './student-report.component.html',
  styleUrls: ['./student-report.component.css']
})
export class StudentReportComponent implements OnInit {

  selectedSemester = '5';
  displayedColumns: string[] = ['courseNo', 'courseName', 'tutor', 'ects', 'grade1', 'grade2', 'grade3', 'paid'];
  dataSource = ELEMENT_DATA;

  constructor() { }

  ngOnInit(): void {
  }

}
