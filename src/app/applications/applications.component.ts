import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Inject } from '@angular/core';

@Component({
  selector: 'app-applications',
  templateUrl: './applications.component.html',
  styleUrls: ['./applications.component.css']
})
export class ApplicationsComponent implements OnInit {

  constructor(@Inject('window') public window: Window) { }

  ngOnInit(): void {
  }

}
