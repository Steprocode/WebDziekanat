import { Component, OnInit } from '@angular/core';

export interface CourseElement {
  courseNo: string;
  courseName: string;
  tutor: string;
  minMaxAttendees: string;
  enrolledStudents: string;
  ects: string;
  semester: string;
  choose: string;
}

const ADDITIONAL_ELEMENT_DATA: CourseElement[] = [];

const ELECTIVE_ELEMENT_DATA: CourseElement[] = [
  {
    courseNo: "25 26421 00",
    courseName: "GUI-programming",
    tutor: "dr inż. Jacek Nowakowski",
    minMaxAttendees: "1/99",
    enrolledStudents: "18",
    ects: "3",
    semester: "5",
    choose: "chosen"
  },
  {
    courseNo: "26 46215 00",
    courseName: "Virtual Reality",
    tutor: "dr inż. Dorota Kamińska",
    minMaxAttendees: "1/99",
    enrolledStudents: "18",
    ects: "3",
    semester: "5",
    choose: "chosen"
  }
];

@Component({
  selector: 'app-student-courses',
  templateUrl: './student-courses.component.html',
  styleUrls: ['./student-courses.component.css']
})
export class StudentCoursesComponent implements OnInit {

  displayedAdditionalColumns: string[] = ['courseNo', 'courseName', 'tutor', 'minMaxAttendees', 'enrolledStudents', 'ects', 'semester', 'choose'];
  additionalDataSource = ADDITIONAL_ELEMENT_DATA;

  displayedElectiveColumns: string[] = ['courseNo', 'courseName', 'tutor', 'minMaxAttendees', 'enrolledStudents', 'ects', 'semester', 'choose'];
  electiveDataSource = ELECTIVE_ELEMENT_DATA;

  constructor() { }

  ngOnInit(): void {
  }

}
