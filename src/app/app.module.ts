import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Inject } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavigationComponent } from './navigation/navigation.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu';
import { MatExpansionModule } from '@angular/material/expansion';
import { StudentComponent } from './student/student.component';
import { TermsComponent } from './terms/terms.component';
import { CourseCatalogueComponent } from './course-catalogue/course-catalogue.component';
import { RegulationsComponent } from './regulations/regulations.component';
import { ApplicationsComponent } from './applications/applications.component';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { MatIconRegistry } from '@angular/material/icon';

import { DomSanitizer } from '@angular/platform-browser';
import { StudentGeneralComponent } from './student-general/student-general.component';
import { StudentReportComponent } from './student-report/student-report.component';
import { StudentCoursesComponent } from './student-courses/student-courses.component';
import { StudentFeesComponent } from './student-fees/student-fees.component';
import { StudentRecordsComponent } from './student-records/student-records.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

export function getWindow() { return window; }
@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    DashboardComponent,
    StudentComponent,
    TermsComponent,
    CourseCatalogueComponent,
    RegulationsComponent,
    ApplicationsComponent,
    StudentGeneralComponent,
    StudentReportComponent,
    StudentCoursesComponent,
    StudentFeesComponent,
    StudentRecordsComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatExpansionModule,
    MatSelectModule,
    MatTableModule
  ],
  providers: [{ provide: 'window', useFactory: getWindow }],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(@Inject('window') window: Window) {

  }
}